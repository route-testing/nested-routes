import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _3f0d4f0e = () => interopDefault(import('../pages/catalog.vue' /* webpackChunkName: "pages/catalog" */))
const _05f9d311 = () => interopDefault(import('../pages/catalog/index.vue' /* webpackChunkName: "pages/catalog/index" */))
const _d507a0c8 = () => interopDefault(import('../pages/catalog/_category.vue' /* webpackChunkName: "pages/catalog/_category" */))
const _1d75911f = () => interopDefault(import('../pages/catalog/_category/index.vue' /* webpackChunkName: "pages/catalog/_category/index" */))
const _772e5462 = () => interopDefault(import('../pages/catalog/_category/_subCategory.vue' /* webpackChunkName: "pages/catalog/_category/_subCategory" */))
const _87e2ed36 = () => interopDefault(import('../pages/catalog/_category/_subCategory/index.vue' /* webpackChunkName: "pages/catalog/_category/_subCategory/index" */))
const _6809a10d = () => interopDefault(import('../pages/catalog/_category/_subCategory/_id.vue' /* webpackChunkName: "pages/catalog/_category/_subCategory/_id" */))
const _2ac4a687 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/catalog",
    component: _3f0d4f0e,
    children: [{
      path: "",
      component: _05f9d311,
      name: "catalog"
    }, {
      path: ":category",
      component: _d507a0c8,
      children: [{
        path: "",
        component: _1d75911f,
        name: "catalog-category"
      }, {
        path: ":subCategory",
        component: _772e5462,
        children: [{
          path: "",
          component: _87e2ed36,
          name: "catalog-category-subCategory"
        }, {
          path: ":id",
          component: _6809a10d,
          name: "catalog-category-subCategory-id"
        }]
      }]
    }]
  }, {
    path: "/",
    component: _2ac4a687,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
